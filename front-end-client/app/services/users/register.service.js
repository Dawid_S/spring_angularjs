class RegisterService {

    constructor($http, $mdDialog) {
        this.$http = $http;
        this.dataUrl = 'http://localhost:8082';
        this.$mdDialog = $mdDialog;
    }

    userRegister(user){
        console.log(user);
            return this.$http({
                method: 'POST',
                url: this.dataUrl + '/register',
                data: user
            });
    }
}
export default RegisterService;