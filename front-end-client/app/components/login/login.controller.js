class LoginController {

    constructor($scope, $state, loginService) {
        this.$state = $state;
       this.$scope = $scope;
       this.loginService = loginService;
       this.credentials = null;

    }

    login() {
       console.log(this.credentials);
       this.loginService.authenticate(this.credentials.username, this.credentials.password).then(
           () => {
               this.$state.go("car");
               console.log('OK')
           },
           () => {
               console.log('Error')
           }
       )
    }
}
LoginController.$inject = ['$scope','$state', 'loginService'];
export default LoginController;