import loginComponent from './login.component';
import loginController from './login.controller'

export default app => {
    app.config(($stateProvider) => {
        $stateProvider
            .state('/login', {
                url: '/login',
                template: '<login></login>',
                controller: loginController
            });
    }).directive('login', loginComponent);
}