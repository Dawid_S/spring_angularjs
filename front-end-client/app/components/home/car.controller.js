import CarDetailController from '../carDetail/carDetail.controller';
import TestModalController from '../testModal/testModal.controller';
import {
    Inject
} from 'angular-annotations';

@Inject('$scope', '$rootScope', '$http', '$mdDialog', 'carsService', 'pdfService')
class CarController {
    constructor($scope, $rootScope, $http ,$mdDialog, carsService, pdfService) {
        this.$rootScope = $rootScope;
        this.$scope = $scope;
        this.$http = $http;
        this.$mdDialog = $mdDialog;
        this.$scope.carsList = null;
        this.$scope.onePageItem = 7;
        this.$scope.currentPage = 1;
        this.$scope.maxSize = 5;
        this.$scope.id = null;
        this.$scope.showTable = true;
        this.$scope.parameters={};
        this.carsService = carsService;
        this.pdfService = pdfService;
        this.initRecords();
    }

    initRecords() {
        this.carsService.getCarList().then(
            (data) => {
                this.$scope.carsList = data.data;
            },
            () =>{
                console.log("Error connection with rest API")
            }
        );
    }


    pageChanged(){
     console.log('page changed to ' + this.$scope.currentPage);
    }


    getCarDetails(id){
        this.carsService.getCarDetail(id).then(
            (data) => {
                this.$scope.carsList = data.data;
            },
            () =>{
                console.log("Error")
            }
        )
    }

    showAdvanced(id){
        /*
        Jako odbiorca service - singleton, $broadcast nie można nadawać bez odbiorcy
        (którym nie moze być controller, gdyż wywoływany jest później).
        this.$scope.test = {
            type: 'listing',
            id: 2
        };
        this.$rootScope.$broadcast("Test", this.$scope.test);
       */

        if (this.dialogPromise) {
            this.$mdDialog.hide();
            this.dialogPromise = null;
        } else {
            this.dialogPromise = this.$mdDialog.show({
                controller: CarDetailController,
                parent: angular.element(document.body),
                templateUrl: '../components/carDetail/carDetail.html',
                controllerAs: 'ctrl',
                targetEvent: id,
                clickOutsideToClose:true,
                escapeToClose: true,
                resolve: {
                   id: function(){
                        console.log(id);
                    return id;
                }
                }
            });

            this.dialogPromise.finally(() => {
                this.dialogPromise = null;
            });
        }
    }

    showTestModal(){
        if (this.dialogPromise) {
            this.$mdDialog.hide();
            this.dialogPromise = null;
        } else {
            this.dialogPromise = this.$mdDialog.show({
                controller: TestModalController,
                parent: angular.element(document.body),
                templateUrl: '../components/testModal/testModal.html',
                controllerAs: 'testCtrl',
                clickOutsideToClose:true,
                escapeToClose: true,
                resolve: {}
            });

            this.dialogPromise.finally(() => {
                this.dialogPromise = null;
            });
        }
    }

    showTestModal2(){
        if (this.dialogPromise) {
            this.$mdDialog.hide();
            this.dialogPromise = null;
        } else {
            this.dialogPromise = this.$mdDialog.show({
                controller: TestModalController,
                parent: angular.element(document.body),
                templateUrl: '../components/testModal/testModal2.html',
                controllerAs: 'testCtrl',
                clickOutsideToClose:true,
                escapeToClose: true,
                resolve: {}
            });

            this.dialogPromise.finally(() => {
                this.dialogPromise = null;
            });
        }
    }

    viewTable(){
        return this.$scope.showTable ? "../components/home/carTable.html" : "";
    }

    openPdf(str){
        this.pdfService.create(str);
    }

}
// CarController.$inject = ['$scope', '$rootScope', '$http', '$mdDialog', 'carsService'];
export default CarController;











