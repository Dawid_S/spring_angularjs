import carComponent from './home/car';
import carDetailComponent from './carDetail/carDetail'
import addCarComponent from './carDetail/addCar'
import sellerComponent from "./seller/seller";
import sellerDetailComponent from "./sellerDetail/sellerDetail";
import loginComponent from "./login/login";
import registerComponent from "./register/register";
import testModalComponent from "./testModal/testModal"

export default app => {
  INCLUDE_ALL_MODULES([carComponent, sellerComponent, carDetailComponent, sellerDetailComponent, addCarComponent, loginComponent, registerComponent, testModalComponent], app);
}
