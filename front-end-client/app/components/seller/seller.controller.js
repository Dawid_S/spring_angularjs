import SellerDetailController from '../sellerDetail/sellerDetail.controller';

class SellerController {
    constructor($scope, $http ,$mdDialog, sellersService) {
        this.$scope = $scope;
        this.$http = $http;
        this.$mdDialog = $mdDialog;
        this.$scope.sellersList = null;
        this.$scope.id = null;
        this.sellersService = sellersService;
        this.initRecords();
    }

    initRecords() {
        this.sellersService.getSellerList().then(
            (data) => {
                this.$scope.sellersList = data.data;
            },
            () =>{
                console.log("Error connection with rest")
            }
        );
    }

    showAdvanced(id){
        if (this.dialogPromise) {
            this.$mdDialog.hide();
            this.dialogPromise = null;
        } else {
            this.dialogPromise = this.$mdDialog.show({
                controller: SellerDetailController,
                parent: angular.element(document.body),
                templateUrl: '../components/sellerDetail/sellerDetail.html',
                controllerAs: 'sellDetCtrl',
                targetEvent: id,
                clickOutsideToClose:true,
                escapeToClose: true,
                resolve: {
                    id: function(){
                        console.log(id);
                        return id;
                    }
                }
            });

            this.dialogPromise.finally(() => {
                this.dialogPromise = null;
            });
        }
    }

}
SellerController.$inject = ['$scope', '$http', '$mdDialog', 'sellersService'];
export default SellerController;











