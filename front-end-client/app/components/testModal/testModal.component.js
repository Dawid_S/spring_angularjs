import template from './testModal.html';
import controller from './testModal.controller';

let testModalComponent = function () {
    return {
        restrict: 'EA',
        scope: {},
        template: template,
        controller: controller,
        controllerAs: 'testCtrl',
        bindToController: true
    };
};
export default testModalComponent;