import template from './sellerDetail.html';
import controller from './sellerDetail.controller';

let sellerDetailComponent = function () {
    return {
        restrict: 'EA',
        scope: {},
        template: template,
        controller: controller,
        controllerAs: 'sellDetCtrl',
        bindToController: true
    };
};
export default sellerDetailComponent;